class GameOfLife

  attr_reader :alive_cells

  def initialize(seed=[])
    @alive_cells = seed
  end

  def next
    @alive_cells = survivors + births
  end

  def survivors
    @alive_cells.select { |cell| alive_neighbours(cell).count.between?(2,3) }
  end
  
  def alive_neighbours(cell)
    neighbours(cell) & @alive_cells
  end

  def births
    @alive_cells.flat_map do |cell|
      dead_neighbours(cell).select { |neighbour| alive_neighbours(neighbour).count == 3}
    end.uniq
  end

  def dead_neighbours(cell)
    neighbours(cell) - @alive_cells   
  end

  def neighbours(cell)

    x, y = cell

    delta = [
      [-1, 1], [ 0, 1], [ 1, 1],
      [-1, 0],          [ 1, 0],
      [-1,-1], [ 0,-1], [ 1,-1]
    ]

    delta.map do |dx, dy| 
      [x + dx, y + dy]
    end

  end

end