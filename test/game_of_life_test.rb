require "test/unit"

require_relative "../lib/game_of_life"

class TestGameOfLife < Test::Unit::TestCase

  def test_blank_board
    game = GameOfLife.new

    assert_equal([], game.next)
  end

  def test_game_stores_alive_cells
    seed = [[0,0]]
    game = GameOfLife.new(seed)

    assert_equal(seed, game.alive_cells)
  end

  def test_blank_board_with_origin_seed
    game = GameOfLife.new([[0,0]])

    assert_equal([], game.next)

  end

  def test_alive_neighbours
    seed = [[0,0]]
    game = GameOfLife.new(seed)

    assert_equal([], game.alive_neighbours([0,0]))    
  end

  def test_alive_neighbours_with_two_alive
    game = GameOfLife.new([
      [0,0],
      [1,0],
      [1,1]
      ])

    assert_equal([[1,0],[1,1]].sort.uniq, game.alive_neighbours([0,0]).sort.uniq)
  end

  def test_neighbours_in_origin
    origin = [0,0]
    game = GameOfLife.new([origin])
    neighbours = game.neighbours(origin)

    assert_equal(8, neighbours.size)
    assert(!neighbours.include?([0,0]))
    assert(neighbours.include?([1,0]))
    assert(neighbours.include?([-1,-1]))
  end

  def test_survivors

    # .**.
    # .**.

    seed = [
      [0,0],
      [0,1],
      [1,0],
      [1,1]
    ]

    game = GameOfLife.new(seed)

    assert_equal(seed, game.next)
    assert_equal(seed, game.next)
  end

  def test_dead_neighbours
    seed = [
      [-1, 0],
      [ 1, 1],
      [-1,-1]
    ]

    game = GameOfLife.new(seed)

    expected_dead_neighbours = [
      [-1,  1],
      [ 0,  1],
      [ 1,  0],
      [ 0, -1],
      [ 1, -1],
    ]

    assert_equal(expected_dead_neighbours.sort, game.dead_neighbours([0,0]).sort)
    
  end

  def test_births

    # .*
    # **

    game = GameOfLife.new([
      [0,0],
      [1,0],
      [1,1]
      ])

    assert_equal([[0,1]], game.births)
  end

  def test_blinker

    # .*.
    # .*.
    # .*.
    #
    # -->
    #
    # ...
    # ***
    # ...

    step_one = [
      [1,0],
      [1,1],
      [1,2]
    ]

    step_two = [
      [0,1],
      [1,1],
      [2,1]
    ]

    game = GameOfLife.new(step_one)

    assert_equal(step_two.sort, game.next.sort)
    assert_equal(step_one.sort, game.next.sort)
  end

end